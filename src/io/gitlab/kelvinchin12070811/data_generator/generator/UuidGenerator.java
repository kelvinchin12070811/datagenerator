//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator.generator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UuidGenerator extends Generator
{
	private JButton genBtn = new JButton("Generate");
	private JLabel t1 = new JLabel("With bracket");
	private JLabel t2 = new JLabel("Without bracket");
	private JTextField t1Text = new JTextField();
	private JTextField t2Text = new JTextField();
	
	public UuidGenerator()
	{
		genBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		
		var labelSz = new Dimension(125, 12);
		t1.setPreferredSize(labelSz);
		t2.setPreferredSize(labelSz);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalStrut(Generator.OUT_BORDER_SIZE));
		
		var group1 = Box.createHorizontalBox();
		group1.add(t1);
		group1.add(t1Text);
		this.add(group1);
		this.add(Box.createVerticalStrut(Generator.HLINE_GAP));
		
		var group2 = Box.createHorizontalBox();
		group2.add(t2);
		group2.add(t2Text);
		this.add(group2);
		this.add(Box.createVerticalStrut(Generator.HLINE_GAP));
		
		var group3 = new JPanel();
		group3.setLayout(new BorderLayout(Generator.OUT_BORDER_SIZE, 0));
		group3.add(genBtn, BorderLayout.CENTER);
		this.add(group3);
	}
	
	private void generate()
	{
		String uuid = UUID.randomUUID().toString();
		t1Text.setText(String.format("{%s}", uuid));
		t2Text.setText(uuid);
	}
	
	@Override
	public String getTabTitle()
	{
		return "UUID/GUID";
	}	
}
