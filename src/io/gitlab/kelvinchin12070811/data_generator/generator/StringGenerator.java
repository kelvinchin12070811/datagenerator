package io.gitlab.kelvinchin12070811.data_generator.generator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class StringGenerator extends Generator
{
	private JButton genBtn = new JButton("Generate");
	private JCheckBox genLowerCase = new JCheckBox("Lower case", true);
	private JCheckBox genNum = new JCheckBox("Numbers", true);
	private JCheckBox genPunc = new JCheckBox("Punctuation");
	private JCheckBox genUpperCase = new JCheckBox("Upper case", true);
	private JLabel charLabel = new JLabel("Generated string");
	private JLabel sizeLabel = new JLabel("Length");
	private JSlider slide = new JSlider(0, 256, 10);
	private JSpinner spin = new JSpinner();
	private JTextField chars = new JTextField();
	private Random rand = new Random();
	
	public StringGenerator()
	{
		genBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		slide.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				spin.setValue(slide.getValue());
			}
		});
		spin.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!e.getSource().equals(spin))
					return;
				int value = (int) spin.getValue();
				if (value > slide.getMaximum())
					spin.setValue(slide.getMaximum());
				else if (value < slide.getMinimum())
					spin.setValue(slide.getMinimum());
				else
					slide.setValue(value);
			}
		});
		spin.setValue(slide.getValue());
		
		charLabel.setPreferredSize(Generator.LABEL_DIM);
		sizeLabel.setPreferredSize(Generator.LABEL_DIM);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalStrut(Generator.OUT_BORDER_SIZE));
		
		var g1 = Box.createHorizontalBox();
		g1.add(charLabel);
		g1.add(chars);
		this.add(g1);
		this.add(Box.createVerticalStrut(4));
		
		var g2 = Box.createHorizontalBox();
		g2.add(sizeLabel);
		g2.add(slide);
		g2.add(spin);
		this.add(g2);
		this.add(Box.createVerticalStrut(4));
		
		var g3 = Box.createHorizontalBox();
		g3.add(genUpperCase);
		g3.add(Box.createHorizontalGlue());
		g3.add(genLowerCase);
		g3.add(Box.createHorizontalGlue());
		g3.add(genNum);
		g3.add(Box.createHorizontalGlue());
		g3.add(genPunc);
		this.add(g3);
		this.add(Box.createVerticalStrut(4));
		
		var g4 = new JPanel();
		g4.setLayout(new BorderLayout(Generator.OUT_BORDER_SIZE, 0));
		g4.add(genBtn);
		this.add(g4);
	}
	
	@Override
	public String getTabTitle()
	{
		return "Characters";
	}
	
	private int randNum(int min, int max)
	{
		return rand.nextInt((max - min)+ 1) + min;
	}
	
	private void generate()
	{
		Random rand = new Random();
		StringBuilder builder = new StringBuilder();
		int length = slide.getValue();
		if (length == 0) return;
		
		boolean type[] = {
			genUpperCase.isSelected(),
			genLowerCase.isSelected(),
			genNum.isSelected(),
			genPunc.isSelected()
		};
		for (int x = 0; x < length; x++)
		{
			int typeSel = randNum(0, 3);
			while (!type[typeSel])
				typeSel = randNum(0, 3);
			
			char res = 0;
			switch (typeSel)
			{
			case 0:
				res = (char)randNum(65, 90);
				break;
			case 1:
				res = (char)randNum(97, 122);
				break;
			case 2:
				res = (char)randNum(48, 57);
				break;
			case 3:
				int group = randNum(1, 4);
				switch (group)
				{
				case 1:
					res = (char)randNum(33, 47);
					break;
				case 2:
					res = (char)randNum(58, 64);
					break;
				case 3:
					res = (char)randNum(91, 96);
					break;
				case 4:
					res = (char)randNum(123, 126);
					break;
				}
				break;
			}
			builder.append(res);
		}
		chars.setText(builder.toString());
	}
}
