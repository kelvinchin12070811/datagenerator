//***********************************************************************************************************
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//***********************************************************************************************************
package io.gitlab.kelvinchin12070811.data_generator.generator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class RawDataGenerator extends Generator
{
	private JButton genButton = new JButton("Generate");
	private JLabel lengthLebel = new JLabel("Size in bytes");
	private JSlider lengthSlider = new JSlider(0, 524288000);
	private JSpinner lengthSpinner = new JSpinner();
	private Random rand = new Random();
	
	public RawDataGenerator()
	{
		lengthSlider.setValue(512);
		lengthSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				lengthSpinner.setValue(lengthSlider.getValue());
			}
		});
		lengthSpinner.setValue(512);
		lengthSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int value = (int)lengthSpinner.getValue();
				if (value > lengthSlider.getMaximum())
					lengthSpinner.setValue(lengthSlider.getMaximum());
				else if (value < lengthSlider.getMinimum())
					lengthSpinner.setValue(lengthSlider.getMinimum());
				else
					lengthSlider.setValue(value);
			}
		});
		
		lengthLebel.setPreferredSize(new Dimension(90, 12));
		
		genButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalStrut(Generator.OUT_BORDER_SIZE));
		
		var g1 = Box.createHorizontalBox();
		g1.add(lengthLebel);
		g1.add(lengthSlider);
		g1.add(lengthSpinner);
		this.add(g1);
		
		var gBottom = new JPanel();
		gBottom.setLayout(new BorderLayout(Generator.HLINE_GAP, 0));
		gBottom.add(genButton, BorderLayout.SOUTH);
		this.add(gBottom);
	}
	
	@Override
	public String getTabTitle()
	{
		return "Raw data";
	}
	
	private int randNum(int min, int max)
	{
		return rand.nextInt((max - min) + 1) + min;
	}
	
	private void generate()
	{
		JFileChooser saveas = new JFileChooser();
		
		if (saveas.showSaveDialog(this.getParent()) != JFileChooser.APPROVE_OPTION)
			return;
		try
		{
			File tgFile = saveas.getSelectedFile();
			if (tgFile.exists())
			{
				int response = JOptionPane.showConfirmDialog(this.getParent(),
						"The file you selected have already exists. Are you sure to ovewrite it?");
				if (response != JOptionPane.YES_OPTION)
					return;
			}
			FileOutputStream writer = new FileOutputStream(saveas.getSelectedFile());
			
			int size = lengthSlider.getValue();
			for (int i = 0; i < size; i++)
			{
				byte result = (byte)randNum(0, 255);
				writer.write(result);
			}
			
			writer.flush();
			writer.close();
			JOptionPane.showMessageDialog(this.getParent(), "Data generated");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(this.getParent(), e.getMessage());
			e.printStackTrace();
		}
	}
}
